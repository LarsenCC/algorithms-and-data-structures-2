import java.util.Scanner;

public class Izziv10 {

    public static void printMatrix(int[][] m) {
        System.out.printf("%4s", "");
        for (int i = 1; i < m[0].length; i++) {
            System.out.printf("%4d", i);
        }
        System.out.println();

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                System.out.printf("%4d", m[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        int k = sc.nextInt(); 

        sc.close();

        int[][] matrix = new int[n + 1][k + 1];
        for (int i = 0; i <= k; i++) {
            matrix[0][i] = 0;
            matrix[1][i] = 1;
        }

        for (int i = 0; i <= n; i++) {
            matrix[i][0] = i;
            matrix[i][1] = i;
        }

        int min = Integer.MAX_VALUE;
        for (int i = 2; i <= n ; i++) {
            for (int j = 2; j <= k; j++) {
                for (int l = 0; l < i; l++) {
                    int first = matrix[l][j - 1];
                    int second = matrix[i - l - 1][j];
                    int max = Math.max(first, second);
                    // System.out.printf("max from %d and %d is %d\n", first, second, max);
                    if (max < min) {
                        min = max;
                    }
                }
                matrix[i][j] = min + 1;
                min = Integer.MAX_VALUE;
            }
        }

        // printMatrix(matrix);
        System.out.println(matrix[n][k]);
    }
}