import java.util.Scanner;

class Complex {
	double re;
	double im;

    public Complex(double real, double imag) {
        re = real;
        im = imag;
    }

    public String toString() {
    	double tRe = (double)Math.round(re * 100000) / 100000;
    	double tIm = (double)Math.round(im * 100000) / 100000;
        if (tIm == 0) return tRe + "";
        if (tRe == 0) return tIm + "i";
        if (tIm <  0) return tRe + "-" + (-tIm) + "i";
        return tRe + "+" + tIm + "i";
    }

	public Complex conj() {
		return new Complex(re, -im);
	}

    // sestevanje
    public Complex plus(Complex b) {
        Complex a = this;
        double real = a.re + b.re;
        double imag = a.im + b.im;
        return new Complex(real, imag);
    }

    // odstevanje
    public Complex minus(Complex b) {
        Complex a = this;
        double real = a.re - b.re;
        double imag = a.im - b.im;
        return new Complex(real, imag);
    }

    // mnozenje z drugim kompleksnim stevilo
    public Complex times(Complex b) {
        Complex a = this;
        double real = a.re * b.re - a.im * b.im;
        double imag = a.re * b.im + a.im * b.re;
        return new Complex(real, imag);
    }

    // mnozenje z realnim stevilom
    public Complex times(double alpha) {
        return new Complex(alpha * re, alpha * im);
    }

    // reciprocna vrednost kompleksnega stevila
    public Complex reciprocal() {
        double scale = re*re + im*im;
        return new Complex(re / scale, -im / scale);
    }

    // deljenje
    public Complex divides(Complex b) {
        Complex a = this;
        return a.times(b.reciprocal());
    }

    // e^this
    public Complex exp() {
        return new Complex(Math.exp(re) * Math.cos(im), Math.exp(re) * Math.sin(im));
    }

    //potenca komplesnega stevila
    public Complex pow(int k) {

    	Complex c = new Complex(1,0);
    	for (int i = 0; i <k ; i++) {
			c = c.times(this);
		}
    	return c;
    }

}

public class Izziv8 {

    public static Complex[] FTT(Complex[] arr, boolean inv) {
        int len = arr.length;
        if (len == 1) return arr;

        Complex[] ys = new Complex[len/2];
        Complex[] yl = new Complex[len/2];
        Complex[] y = new Complex[len];

        int index = 0;
        for (int i = 0; i < len; i = i + 2) {
            ys[index] = arr[i];
            index++;
        }
        index = 0;
        for (int i = 1; i < len; i = i + 2) {
            yl[index] = arr[i];
            index++;
        }

        ys = FTT(ys, inv);
        yl = FTT(yl, inv);

        Complex omega;
        if (!inv) {
            omega = new Complex(Math.cos(2 * Math.PI / len), Math.sin(2 * Math.PI / len));
        } else {
            omega = new Complex(Math.cos(2 * Math.PI / len), Math.sin(2 * Math.PI / len));
            omega = omega.pow((len - 1));
        }
        
        //System.out.println(omega.toString());
        Complex omegaK = new Complex(1.0, 0);
        for (int i = 0; i <= (len/2) - 1; i++) {
            y[i] = ys[i].plus(yl[i].times(omegaK));
            y[i + (len/2)] = ys[i].minus(yl[i].times(omegaK));
            omegaK = omegaK.times(omega);
        }

        sled(y);
        return y;
    }

    public static void sled(Complex[] y) {
        for (int i = 0; i < y.length; i++) {
            System.out.print(y[i].toString());
            System.out.print(" ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int dol_pol = sc.nextInt();

        int n = (int) Math.ceil(Math.log((double) (dol_pol + dol_pol)) / Math.log(2.0));
        n = (int) Math.pow(2, n);

        Complex[] a = new Complex[n];
        Complex[] b = new Complex[n];
        Complex[] c = new Complex[n];

        for (int i = 0; i < n; i++) {
            if (i < dol_pol) {
                a[i] = new Complex((double) sc.nextInt(), 0.0);
            } else {
                a[i] = new Complex(0.0, 0.0);
            }
        }

        for (int i = 0; i < n; i++) {
            if (i < dol_pol) {
                b[i] = new Complex((double) sc.nextInt(), 0.0);
            } else {
                b[i] = new Complex(0.0, 0.0);
            }
        }

        a = FTT(a, false);
        b = FTT(b, false);

        for (int i = 0; i < n; i++) {
            c[i] = a[i].times(b[i]);
        }

        c = FTT(c, true);

        for (int i = 0; i < n; i++) {
            c[i] = c[i].times((1.0 / n));
        }
        
        sled(c);
    }

}