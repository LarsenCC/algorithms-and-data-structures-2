import java.util.Scanner;

public class Izziv3 {
    
    private static int[] merge(int[] a, int[] b) {
        //MAKE NEW ARRAY WITH THE LENGTH OF BOTH
        int len = a.length + b.length;
        int[] merged = new int[len];
        int k = 0;
        int j = 0;

        //ORDER A TWO ORDERED LISTS IN ONE, JUST KEEP TRACK OF THE INDEX OF THE LAST LOWEST NUMBER
        for (int i = 0; i < len; i++) {
            if ((j == b.length) || ((k != a.length) && (a[k] <= b[j]))) {
                merged[k + j] = a[k];
                k++;
                System.out.print("a");
            } else {    
                merged[k + j] = b[j];
                j++;
                System.out.print("b");
            }
        }

        //RETURN THE MERGED ARRAY
        return merged;
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        int a = sc.nextInt();
        int b = sc.nextInt();
        int[] aa = new int[a];
        int[] bb = new int[b];
        
        for (int i = 0; i < a; i++) {
            aa[i] = sc.nextInt();
        }
        
        for (int i = 0; i < b; i++) {
            bb[i] = sc.nextInt();
        }
        
        int[] merged = merge(aa, bb);
        
        System.out.println();
        
        for (int i = 0; i < merged.length; i++) {
            System.out.print(merged[i] + " ");
        }
    }
    
}