import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class Izziv7 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        int omega = sc.nextInt();
        int n = omega + 1;
        
        HashSet<Integer> set = new HashSet<Integer>();
        ArrayList<Integer> list = new ArrayList<Integer>();

        while (true) {
            if (checkPrime(n)) {
                for (int i = 2; i < n; i++) {
                    for (int l = 2; l <= omega; l++) {
                        int k = (int) potMod(i, l, n);
                        if (k == 1 && l != omega) {
                            set.add(i);
                        } else if (k == 1 && l == omega && !set.contains(i)) {
                            list.add(i);
                        }
                    }
                }
                if (!list.isEmpty()) break;
                list.clear();
                set.clear();
            }
            n++;
        }

        System.out.printf("%d: ", n);
        int[] primitive = listToArray(list);
        // Arrays.sort(primitive);
        for (int i = 0; i < primitive.length; i++) {
            System.out.print(primitive[i] + " ");
        }
        System.out.println();

        int[][] matrix = new int[omega][omega];
        int min_prim = primitive[0];

        for (int i = 0; i < omega; i++) {
            System.out.print(" ");
            for (int j = 0; j < omega; j++) {
                matrix[i][j] =  (int) potMod(min_prim, j*i, n);;
                if (j == 0) {
                    System.out.print(matrix[i][j]);
                } else {
                    System.out.printf("%3d", matrix[i][j]);
                }
            }
            System.out.println();
        }
    }

    public static boolean checkPrime(int a) {
        for (int i = 2; i <= a / 2; i++) {
            if (a % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static int[] listToArray(ArrayList<Integer> list) {
        int[] a = new int[list.size()];
        int index = 0; 
        for (int i : list) {
            a[index] = i;
            index++;
        }
        return a;
    }

    // MODUL Z AVELIKE POTENCE
    public static long potMod(int a, int b, int c) {
        b = b % (c - 1); // ker ne rab vseh gledat ... ponavljajo se vrednosti!

		if (b == 0) {
			return 1;
		} else if (b == 1) {
			return a % c;
		} else {
			long rez = a % c;
			for (int i = 2; i <= b; i++){
				rez = (rez * (a % c)) % c;
            }
            
			return rez;
		} 
	}

}