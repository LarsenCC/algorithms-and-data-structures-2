import java.util.Scanner;

public class Izziv5 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String[] line = sc.nextLine().split(" ");
        int[] a = new int[line.length];

        for (int i = 0; i < a.length; i++) {
            a[i] = Integer.valueOf(line[i]);
        }

        biggestSubArraySum(a, 0, a.length -1);
    }

    public static int biggestSubArraySum(int[] arr, int left, int right) {
        if ((right - left) < 1) {
            printArray(arr, left, right);
            System.out.println(arr[right]);
            return arr[right];
        } else {
            int len = right - left;
            int split = (right + left)/2;

            int sum1 = biggestSubArraySum(arr, left, split);
            int sum2 = biggestSubArraySum(arr, split + 1, right);

            int c = arr[split + 1];
            int sum = 0, max = c;
            for (int i = split; i >= left; i--) {
                sum = sum + arr[i];
                if (sum + c > max) {
                    max = sum + c;
                }
            }

            sum = 0; 
            c = max;
            for (int i = split + 2; i <= right; i++) {
                sum = sum + arr[i];
                if (sum + c > max) {
                    max = sum + c;
                }
            }

            printArray(arr, left, right);
            System.out.println(Math.max(max, Math.max(sum1, sum2)));
            return Math.max(max, Math.max(sum1, sum2));
        }
    }

    private static void printArray(int[] a, int left, int right) {
        System.out.print("[");
        for (int i = left; i <= right; i++) {
            if (i != right) System.out.print(a[i] + ", ");
            else System.out.print(a[i]);
        }
        System.out.print("]: ");
    }
}