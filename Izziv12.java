import java.util.Scanner;

import sun.net.www.content.audio.basic;

public class Izziv12 {

    public static void printHops(int index, int[][] hops) {
        System.out.printf("h%d:", index);
        for (int i = 0; i < hops[index].length; i++) {
            if (hops[index][i] == Integer.MAX_VALUE) {
                System.out.printf(" inf");
            } else {
                System.out.printf(" %d", hops[index][i]);
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num_of_verticies = sc.nextInt();

        int[][] graph = new int[num_of_verticies][num_of_verticies];
        int[][] hops = new int[num_of_verticies][num_of_verticies];

        // get input
        while (sc.hasNextInt()) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            int value = sc.nextInt();

            graph[from][to] = value;
        }

        // initialize hops
        for (int i = 0; i < num_of_verticies; i++) {
            for (int j = 0; j < num_of_verticies; j++) {
                if (j == 0) {
                    hops[i][j] = 0;
                } else {
                    hops[i][j] = Integer.MAX_VALUE;
                }
            }
        }

        printHops(0, hops);

        // Bellman-Ford
        for (int i = 1; i < num_of_verticies; i++) {
            // current vertex
            for (int j = 0; j < num_of_verticies; j++) {
                // to vertex
                for (int k = 0; k < num_of_verticies; k++) {

                    if (k != j && hops[i-1][k] != Integer.MAX_VALUE && graph[k][j] != 0) {
                        hops[i][j] = Math.min(Math.min(hops[i][j], hops[i-1][j]), hops[i-1][k] + graph[k][j]);
                    }

                }
            }

            printHops(i, hops);
        }
    }

}