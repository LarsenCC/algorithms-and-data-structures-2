import java.util.Scanner;

public class Izziv11 {

    public static void printResult(int idx, int[] res) {
        System.out.print(idx + ": ");
        for (int i = 0; i < res.length; i++) {
            if (res[i] != -1)
                System.out.printf("(%d, %d) ", i, res[i]);
        }
        System.out.println();
    }

    public static boolean isBest(int[] best, int val, int vol) {
        for (int i = 1; i < vol; i++) {
            if (best[i] >= val)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int w = sc.nextInt();
        int n = sc.nextInt();

        int[][] stuff = new int[n + 1][2];
        int[] best = new int[w + 1];
        int[] current = new int[w + 1];

        for (int i = 1; i <= n; i++) {
            stuff[i][0] = sc.nextInt();
            stuff[i][1] = sc.nextInt();
        }

        for (int i = 1; i <= w; i++) {
            best[i] = -1;
            current[i] = -1;
        }

        best[0] = 0;
        printResult(0, best);
        for (int i = 1; i <= n; i++) {
            int vol = stuff[i][0];
            int val = stuff[i][1];

            for (int j = 0; j <= w; j++) {
                if (vol > w) {
                    break;
                }

                if (best[j] != -1 && j + vol <= w && isBest(best, val + best[j], j + vol)) {
                    current[j + vol] = best[j] + val;
                }
            }

            for (int j = 0; j <= w; j++) {
                if (current[j] == -1) {
                    continue;
                }
                for (int k = j + 1; k <= w; k++) {
                    if (best[k] <= current[j]) {
                        best[k] = -1;
                    }
                }
                best[j] = current[j];
                current[j] = -1;
            }

            printResult(i, best);
        }
    }
}