import java.util.Scanner;

public class Izziv4 {

    public static void main(String[] args) {        
        Scanner sc = new Scanner(System.in);
        
        int n = sc.nextInt();
        int[] table = new int[n];
        int[] c = new int[32 + 1];
        int[] b = new int[n];

        // branje in stetje
        for (int i = 0; i < n; i++) {
            table[i] = sc.nextInt();
            c[getNumOfOnes(table[i])]++;
        }

        // komulativa
        for (int i = 1; i <= 32; i++) {
            c[i] = c[i - 1] + c[i];
        }

        // prepis
        for (int i = n - 1; i >= 0; i--) {
            int index = c[getNumOfOnes(table[i])] - 1;
            c[getNumOfOnes(table[i])]--;
            int num = table[i];
            b[index] = num;
            System.out.printf("(%d,%d)\n", num,index);
        }

        // izpis
        for (int i = 0; i < n; i++) {
            System.out.print(b[i] + " ");
        }
    }

    public static int getNumOfOnes(int n) {
        int c = 0;
        while(n > 0) {
            c += (n % 2);
            n /= 2;
        }
        return c;
    }

}