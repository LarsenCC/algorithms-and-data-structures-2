import java.util.Scanner;

class VertexTag {
    private int from; // from which vertex did we get here
    private char type; // + or -
    private int capacity; // how much were we able to push through

    public VertexTag(int from, char type, int capacity) {
        this.from = from;
        this.type = type;
        this.capacity = capacity;
    }

    public int getFrom() {
        return this.from;
    }

    public char getType() {
        return this.type;
    }

    public int getCapacity() {
        return this.capacity;
    }
}

public class Izziv9 {

    public static void printPath(VertexTag[] tags, int[][][] graph) {
        int current_vertex = tags.length - 1;
        int capacity = tags[current_vertex].getCapacity();
        System.out.printf("%d: ", capacity);
        for (int i = 0; i < tags.length; i++) {
            // print where we are and how we got there ...
            // change previous path capacity ...
            if (tags[current_vertex].getType() == '+') {
                System.out.printf("%d%c  ", current_vertex, tags[current_vertex].getType());
                graph[tags[current_vertex].getFrom()][current_vertex][0] += capacity;
            } else {
                System.out.printf("%d%c ", current_vertex, tags[current_vertex].getType());
                graph[current_vertex][tags[current_vertex].getFrom()][0] -= capacity; 
            }
            // set new current vertex to FROM
            current_vertex = tags[current_vertex].getFrom();
            if (current_vertex == 0) break;
        }
        System.out.println("0");
    }

    public static void printGraph(int[][][] graph) {
        for (int i = 0; i < graph.length; i++) {
            System.out.printf("%2d: ", i);
            for (int j = 0; j < graph[i].length; j++) {
                System.out.printf("%-2d/%2d  ", graph[i][j][0], graph[i][j][1]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        int num_of_verticies = sc.nextInt();
        int[][][] graph = new int[num_of_verticies][num_of_verticies][2];
        while (sc.hasNextInt()) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            int capacity = sc.nextInt();

            graph[from][to][0] = 0;
            graph[from][to][1] = capacity;
        }

        // printGraph(graph);

        VertexTag[] tags = new VertexTag[num_of_verticies];
        boolean[] visited = new boolean[num_of_verticies];

        boolean can_get_to_end = true;
        while (can_get_to_end) {
            // we always start at 0
            int current_vertex = 0;
            // -1 is only at tha beginning, we dont care what it is ...
            tags[0] = new VertexTag(-1, '+', Integer.MAX_VALUE);
            visited = new boolean[num_of_verticies];

            while (current_vertex != (num_of_verticies - 1)) {
                // we just visited current vertex
                visited[current_vertex] = true;

                if (tags[num_of_verticies - 1] != null) {
                    // we found a path to the final vertex
                    break;
                }

                for (int i = 0; i < num_of_verticies; i++) {
                    // we dont need to look at the vertex we are currently on ...
                    if (current_vertex == i) continue;

                    // tag positive path
                    if (graph[current_vertex][i][0] < graph[current_vertex][i][1] && tags[i] == null) {
                        int min = Math.min(tags[current_vertex].getCapacity(), graph[current_vertex][i][1] - graph[current_vertex][i][0]);
                        tags[i] = new VertexTag(current_vertex, '+', min);
                    }

                    // tag negative path
                    if (tags[i] == null && graph[i][current_vertex][0] > 0) {
                        int min = Math.min(tags[current_vertex].getCapacity(), graph[i][current_vertex][0]);
                        tags[i] = new VertexTag(current_vertex, '-', min);
                    }
                }

                boolean found = false;
                for (int i = 0; i < tags.length; i++) {
                    // find a vertex that has been tagged and hasnt been visited yet
                    if (tags[i] != null && !visited[i]) {
                        current_vertex = i;
                        found = true;
                        break;
                    }
                }
                
                if (!found) {
                    can_get_to_end = false;
                    break;
                }
            }

            if (can_get_to_end) {
                printPath(tags, graph);
                // reset tags
                tags = new VertexTag[num_of_verticies];
            }
        }
        
    }
}