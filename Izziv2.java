import java.util.Scanner;

public class Izziv2 {
    
    public static void pogrezni(int[] a, int i, int dolzKopice) {
        int temp = 0;

        if (dolzKopice > (i * 2) + 1) {
            int max = a[i * 2] >= a[(2 * i) + 1] ? a[2 * i] : a[(2 * i) + 1];
            int index = a[i * 2] >= a[(2 * i) + 1] ? 2 * i : (2 * i) + 1;

            if (max > a[i]) {
                temp = a[i];
                a[i] = a[index];
                a[index] = temp;
                pogrezni(a, index, dolzKopice);
            } else {
                return;
            }

        } else if (dolzKopice > i * 2) {
            if (a[i * 2] > a[i]) {
                temp = a[i];
                a[i] = a[i * 2];
                a[i * 2] = temp;
                pogrezni(a, i * 2, dolzKopice);
            } else {
                return;
            }
        } else {
            return;
        }
    }

    public static void zgradiKopico(int[] a) {
        int len = (a.length - 1) / 2;
        for (int i = len; i > 0; i--) {
            pogrezni(a, i, a.length);
        }
    }

    public static void printHeap(int[] a, int len) {
        System.out.print(a[1] + " ");
        for (int i = 2; i < len; i *= 2) {
            System.out.print("| ");
            for (int j = i; j < i * 2; j++) {
                if (j == len) break; 
                System.out.print(a[j] + " ");
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // READ input data ...
        Scanner sc = new Scanner(System.in);
        int d = sc.nextInt();
        int[] a = new int[d + 1];
        for (int i = 1; i <= d; i++) {
            a[i] = sc.nextInt();
        }

        zgradiKopico(a);
        printHeap(a, a.length);
        int len = a.length - 1;

        for (int i = 1; i < a.length - 1; i++) {
            int max = a[1];
            a[1] = a[len];
            a[len] = max;
            pogrezni(a, 1, len);
            printHeap(a, len);
            len--;
        }
    }
}