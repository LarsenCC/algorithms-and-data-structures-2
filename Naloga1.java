import java.util.Scanner;

public class Naloga1 {

    private static int compare = 0, swap = 0;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Boolean trace = sc.next().equals("trace") ? true : false; // else sled
        String alg = sc.next();
        Boolean up = sc.next().equals("up") ? true : false; // else down
        int num = sc.nextInt();

        int[] arr = new int[num];
        for (int i = 0; i < num; i++) {
            arr[i] = sc.nextInt();
        }

        switch (alg) {
            case "bs":
                BubbleSort(arr, trace, up);
                if (!trace) {
                    System.out.println(compare + " " + swap);
                    BubbleSort(arr, trace, up);
                    System.out.println(compare + " " + swap);
                    BubbleSort(arr, trace, !up);
                    System.out.println(compare + " " + swap);
                }   
                break;

            case "ss":
                SelectionSort(arr, trace, up);
                if (!trace) {
                    System.out.println(compare + " " + swap);
                    SelectionSort(arr, trace, up);
                    System.out.println(compare + " " + swap);
                    SelectionSort(arr, trace, !up);
                    System.out.println(compare + " " + swap);
                }
                break;

            case "is":
                InsertionSort(arr, trace, up);
                if (!trace) {
                    System.out.println(compare + " " + swap);
                    InsertionSort(arr, trace, up);
                    System.out.println(compare + " " + swap);
                    InsertionSort(arr, trace, !up);
                    System.out.println(compare + " " + swap);
                }
                break;

            case "hs":

                int[] a = new int[arr.length + 1];
                for (int i = 1; i < a.length; i++) {
                    a[i] = arr[i - 1];
                }

                HeapSort(a, trace, up);
                if (!trace) {
                    System.out.println(compare + " " + swap);
                    HeapSort(a, trace, up);
                    System.out.println(compare + " " + swap);
                    HeapSort(a, trace, !up);
                    System.out.println(compare + " " + swap);
                }
                break;

            case "qs":
                compare = 0; swap = 0;
                QuickSort(arr, trace, up, 0, arr.length - 1);
                if (!trace) {
                    System.out.println(compare + " " + swap);
                    compare = 0; swap = 0;
                    QuickSort(arr, trace, up, 0, arr.length - 1);
                    System.out.println(compare + " " + swap);
                    compare = 0; swap = 0;
                    QuickSort(arr, trace, !up, 0, arr.length - 1);
                    System.out.println(compare + " " + swap);
                }
                break;
            
            case "ms":
                compare = 0; swap = 0;
                MergeSort(arr, trace, up, 0, arr.length - 1);
                if (!trace) {
                    System.out.println(compare + " " + swap);
                    compare = 0; swap = 0;
                    MergeSort(arr, trace, up, 0, arr.length - 1);
                    System.out.println(compare + " " + swap);
                    compare = 0; swap = 0;
                    MergeSort(arr, trace, !up, 0, arr.length - 1);
                    System.out.println(compare + " " + swap);
                }
                break;

            case "cs":
                CountingSort(arr, up, 0);
                break;

            case "rs":
                RadixSort(arr, up);
                break;

        }

    }

    /*
    *   BUBBLESORT
    */

    public static void BubbleSort(int[] arr, boolean trace, boolean up) {
        compare = 0; swap = 0;

        for (int i = arr.length - 1; i >= 0; i--) {
            if (trace) bubbleTrace(arr, i);

            for (int j = arr.length - 1; j > arr.length - 1 - i; j--) {
                if (up && arr[j - 1] > arr[j]) {
                    swap += 3;
                    int tmp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = tmp;
                } else if (!up && arr[j - 1] < arr[j]) {
                    swap += 3;
                    int tmp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = tmp;
                }
                compare++;
            }
        }

        //if (trace) bubbleTrace(arr, 0);
    }

    /*
    *   SELECTIONSORT
    */

    public static void SelectionSort(int[] arr, boolean trace, boolean up) {
        compare = 0; swap = 0;

        for (int i = 0; i < arr.length - 1; i++) {
            if (trace) bubbleTrace(arr, arr.length - 1 - i);

            int value = arr[i];
            int index = i;

            for (int j = i + 1; j < arr.length; j++) {
                if ((up && value > arr[j]) || (!up && value < arr[j])) {
                    index = j;
                    value = arr[j];
                }
                compare++;
            }

            swap += 3;
            int tmp = arr[i];
            arr[i] = arr[index];
            arr[index] = tmp;
        }

        if (trace) bubbleTrace(arr, 0);
    }

    /*
    *   INSERTIONSORT
    */

    public static void InsertionSort(int[] arr, boolean trace, boolean up) {
        compare = 0; swap = 0;

        for (int i = 1; i < arr.length; i++) {
            if (trace) bubbleTrace(arr, arr.length - i - 1);

            for (int j = i; j > 0; j--) {
                compare++;
                if ((up && arr[j - 1] > arr[j]) || (!up && arr[j - 1] < arr[j])) {
                    swap += 3;
                    int tmp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = tmp;
                } else if ((up && arr[j - 1] <= arr[j]) || (!up && arr[j - 1] >= arr[j])) {
                    break;
                }
            }
            
        }

        if (trace) bubbleTrace(arr, -1);
    }

    // applicable for bubble, selection and insertion sort
    private static void bubbleTrace(int[] arr, int i) {
        int index = arr.length - i - 1;
        for (int k = 0; k < arr.length; k++) {
            if (k == index) {
                System.out.print("| ");
            }
            System.out.print(arr[k] + " ");
        }
        if (index == arr.length) System.out.print("| ");

        System.out.println();
    }   


    /*
    *   HEAPSORT
    */

    public static void HeapSort(int[] arr, boolean trace, boolean up) {
        zgradiKopico(arr, up);
        if (trace) heapTrace(arr, arr.length);
        int len = arr.length - 1;

        for (int i = 1; i < arr.length - 1; i++) {
            swap += 3;
            int value = arr[1];
            arr[1] = arr[len];
            arr[len] = value;
            pogrezni(arr, 1, len, up);
            if (trace) heapTrace(arr, len);
            len--;
        }
    }

    private static void pogrezni(int[] a, int i, int dolzKopice, boolean up) {
        int temp = 0;

        if (dolzKopice > (i * 2) + 1) {
            int value = 0;
            int index = 0;
            if (up) {
                value = a[i * 2] >= a[(2 * i) + 1] ? a[2 * i] : a[(2 * i) + 1];
                index = a[i * 2] >= a[(2 * i) + 1] ? 2 * i : (2 * i) + 1;
            } else {
                value = a[i * 2] <= a[(2 * i) + 1] ? a[2 * i] : a[(2 * i) + 1];
                index = a[i * 2] <= a[(2 * i) + 1] ? 2 * i : (2 * i) + 1;
            }
            compare++;

            if ((up && value > a[i]) || (!up && value < a[i])) {
                swap += 3;
                temp = a[i];
                a[i] = a[index];
                a[index] = temp;
                compare++;
                pogrezni(a, index, dolzKopice, up);
            } else {
                compare++;
                return;
            }

        } else if (dolzKopice > i * 2) {
            if ((up && a[i * 2] > a[i]) || (!up && a[i * 2] < a[i])) {
                swap += 3;
                temp = a[i];
                a[i] = a[i * 2];
                a[i * 2] = temp;
                compare++;
                pogrezni(a, i * 2, dolzKopice, up);
            } else {
                compare++;
                return;
            }
        } else {
            return;
        }
    }

    // buildheap
    private static void zgradiKopico(int[] arr, boolean up) {
        compare = 0; swap = 0;

        int len = (arr.length - 1) / 2;
        for (int i = len; i > 0; i--) {
            pogrezni(arr, i, arr.length, up);
        }
    }

    // trace heap
    private static void heapTrace(int[] arr, int len) {
        System.out.print(arr[1] + " ");
        for (int i = 2; i < len; i *= 2) {
            System.out.print("| ");
            for (int j = i; j < i * 2; j++) {
                if (j == len) break; 
                System.out.print(arr[j] + " ");
            }
        }
        System.out.println();
    }

    /*
    *   QUICKSORT
    */

    public static void QuickSort(int[] arr, boolean trace, boolean up, int left, int right) {
        if (right > left) {
            int[] p = partition(arr, left, right, up);
            //System.out.println("p[0] = " + p[0] + ", p[1] = " + p[1]);
            if (trace) quickTrace(arr, right - left, left, p[0] + 1, p[1]);
            
            QuickSort(arr, trace, up, left, p[0]);
            QuickSort(arr, trace, up, p[0] + 1, p[1] - 1);
            QuickSort(arr, trace, up, p[1], right);
        }
        //return arr;
    }

    private static int[] partition(int[] arr, int left, int right, boolean up) {
        int i = left, j = right;
        int pivot = arr[(right + left) / 2];
        swap++;
        while (i <= j) {
            while ((up && arr[i] < pivot) || (!up && arr[i] > pivot)) {i++; compare++;}
            while ((up && arr[j] > pivot) || (!up && arr[j] < pivot)) {j--; compare++;}
            compare += 2;
            if (i <= j) {
                swap(arr, i, j);
                i++; j--;
            }
        }
        int[] p = new int[2];
        p[0] = j;
        p[1] = i;
        return p;
    }

    private static void swap(int[] arr, int i, int j) {
        swap += 3;
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
    }

    private static void quickTrace(int[] arr, int len, int start, int i1, int i2) {
        for (int k = start; k <= start + len; k++) {
            if (k == i1) {
                System.out.print("| ");
            }
            if (k == i2) {
                System.out.print("| ");
            }
            System.out.print(arr[k] + " ");
        }

        System.out.println();
    } 
    
    /*
    *   MERGESORT
    */

    public static void MergeSort(int[] arr, boolean trace, boolean up, int left, int right) {
        if ((right - left) < 1) {
            swap++;
            return;
        } else {
            int len = right - left;
            int split = (right + left)/2;
            if (trace) mergeTrace(arr, len, left, false);

            MergeSort(arr, trace, up, left, split);
            MergeSort(arr, trace, up, split + 1, right);

            merge(arr, left, right, split, trace, up);
        }
    }

    private static void merge(int[]arr, int left, int right, int split, boolean trace, boolean up) {
        int i = left, j = split + 1;
        int index = 0;
        int[] merged = new int[right - left + 1];

        for (int k = left; k <= right; k++) {
            if (j == right + 1) {
                merged[index] = arr[i];
                i++;
            } else if ((up && i != split + 1 && arr[i] <= arr[j]) || (!up && i != split + 1 && arr[i] >= arr[j])) {
                merged[index] = arr[i];
                compare++;
                i++;
            } else if (i == split + 1) {
                merged[index] = arr[j];
                j++;
            } else {
                compare++;
                merged[index] = arr[j];
                j++;
            }
            
            swap++;
            index++;
        }

        if (trace) mergeTrace(merged, merged.length - 1, 0, true);

        for (int k = left; k <= right; k++) {
            //if (arr[k] != merged[k - left]) swap++;
            arr[k] = merged[k - left];
            //swap++;
        }
    }

    private static void mergeTrace(int[] arr, int len, int i, boolean merge) {
        for (int k = i; k <= i + len; k++) {
            if ((k == 1 + i + len/2) && !merge) {
                System.out.print("| ");
            }
            System.out.print(arr[k] + " ");
        }

        System.out.println();
    } 

    /*
    *   COUNTINGSORT
    */

    public static void CountingSort(int[] arr, boolean up, int bt) {
        int[] c = new int[256];
        int[] b = new int[arr.length];

        // stetje
        for (int i = 0; i < arr.length; i++) {
            c[(arr[i] & (255 << (bt * 8))) >> (bt * 8)]++;
        }

        // komulativa
        if (up) {
            for (int i = 1; i <= 255; i++) {
                c[i] = c[i - 1] + c[i];
            }
        } else {
            // POPRAVI
            for (int i = 255; i >= 1; i--) {
                c[i - 1] = c[i - 1] + c[i];
            }
        }
        // izpis tabele s komulativo
        for (int i = 0; i < c.length; i++) {
            System.out.print(c[i] + " ");
        }
        System.out.println();

        // prepis

        for (int i = arr.length - 1; i >= 0; i--) {
            int index = c[(arr[i] & (255 << (bt * 8))) >> (bt * 8)] - 1;
            c[(arr[i] & (255 << (bt * 8))) >> (bt * 8)]--;
            int num = arr[i];
            b[index] = num;
            System.out.printf("%d ", index);
        }
        System.out.println();

        // izpis
        for (int i = 0; i < arr.length; i++) {
            System.out.print(b[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < arr.length; i++) {
            arr[i] = b[i];
        }
    }

    /*
    *   RADIXSORT
    */

    public static void RadixSort(int[] arr, boolean up) {
        CountingSort(arr, up, 0);
        CountingSort(arr, up, 1);
        CountingSort(arr, up, 2);
        CountingSort(arr, up, 3);
        //CountingSort(arr, up, postfix);
    }
}