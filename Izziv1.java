import java.util.Arrays;

public class Izziv1 {

    public static int[] generateTable(int n) {
        int[] table = new int[n];
        for (int i = 0; i < n; i++) {
            //table[i] = (int) (Math.random() * n);
            table[i] = i;
        }
        //Arrays.sort(table);

        return table;
    }

    public static int findLinear(int[] a, int v) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == v) {
                return i;
            }
        }
        return -1;
    }

    public static int findBinary(int[] a, int l, int r, int v) {
        while (l <= r) {
            int middle = l + ((r - l) / 2);
            if (a[middle] == v) {
                return middle;
            } else if (a[middle] > v) {
                r = middle - 1;
            } else {
                l = middle + 1;
            }
        }
        return -1;
    }

    public static void makeLine() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 20; j++) {
                System.out.print("-");
            }
            System.out.print("+");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        System.out.printf("%20s|%20s|%20s|\n", "n", "linearno", "dvojisko");
        makeLine();

        long startTimeL, executionTimeL;
        long startTimeB, executionTimeB;
        long avgL = 0;
        long avgB = 0;
        
        for (int i = 1000; i <= 100000; i = i + 1000) {
            for (int j = 0; j < 1000; j++) {
                int[] test = generateTable(i);
                int random = (int) (Math.random() * i);
    
                // LINEARNO
                startTimeL = System.nanoTime();
                findLinear(test, random);
                executionTimeL = System.nanoTime() - startTimeL;
    
                // BINARNO
                startTimeB = System.nanoTime();
                findBinary(test, 0, i - 1, random);
                executionTimeB = System.nanoTime() - startTimeB;
    
                avgL = avgL + executionTimeL;
                avgB = avgB + executionTimeB;
    
                //System.out.printf("%20d|%20d|%20d|\n", i, executionTimeL, executionTimeB);
            }

            System.out.printf("%20d|%20d|%20d|\n", i, avgL/1000, avgB/1000);
            avgB = 0;
            avgL = 0;
        }

        makeLine();

        //System.out.printf("%20s|%20d|%20d|\n", "average --> ", avgL / 1000, avgB / 1000);

        /*int[] test = generateTable(1000000);
        System.out.println(Arrays.toString(test));
        int random = (int) (Math.random() * 1000000);
        System.out.println(random);

        startTimeB = System.nanoTime();
        findBinary(test, 0, 1000000 - 1, random);
        executionTimeB = System.nanoTime() - startTimeB;

        System.out.printf("%20d|%20d|%20d|\n", 1000000, 0, executionTimeB);*/

        // TESTI

        //int[] test = generateTable(100);
        //System.out.println(Arrays.toString(test));
        /*for (int i = 0; i < 101; i++) {
            System.out.println("Bi: " + findBinary(test, 0, test.length - 1, i));
            System.out.println("Li: " + findLinear(test, i));
        }
        System.out.println("Bi: " + findBinary(test, 0, test.length - 1, 0));
        System.out.println("Li: " + findLinear(test, 0));*/
    }

}