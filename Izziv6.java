import java.util.Scanner;

class Matrix {

	private int[][] m;

	public int n; //only square matrices

	public Matrix(int n){

		this.n = n;

		m = new int[n][n];

	}

    //set value at i,j
	public void setV(int i, int j, int val){

		m[i][j] = val;

	}

    // get value at index i,j
	public int v(int i, int j){

		return m[i][j];

	}

    // return a square submatrix from this
	public Matrix getSubmatrix(int startRow, int startCol, int dim){

		Matrix subM = new Matrix(dim);

		for (int i = 0; i<dim ; i++ )

			for (int j=0;j<dim ; j++ )

				subM.setV(i,j, m[startRow+i][startCol+j]);



		return subM;

	}

    // write this matrix as a submatrix from b (useful for the result of multiplication)
	public void putSubmatrix(int startRow, int startCol, Matrix b){

		for (int i = 0; i<b.n ; i++ )

			for (int j=0;j<b.n ; j++ )

				setV(startRow+i,startCol+j, b.v(i,j));

	}

    // matrix addition
	public Matrix sum(Matrix b){

		Matrix c = new Matrix(n);

		for(int i = 0; i< n;i++){

			for(int j = 0; j<n;j++){

				c.setV(i, j, m[i][j]+b.v(i, j));

			}

		}

		return c;
	}

    // matrix subtraction
	public Matrix sub(Matrix b){

		Matrix c = new Matrix(n);

		for(int i = 0; i< n;i++){

			for(int j = 0; j<n;j++){

				c.setV(i, j, m[i][j]-b.v(i, j));

			}

		}

		return c;

	}

	//simple multiplication
	public Matrix mult(Matrix b){
		Matrix c = new Matrix(n);

        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                for (int k = 0; k < this.n; k++) {
                    int val = this.v(i, k)*b.v(k, j);
                    c.setV(i, j, c.v(i, j) + val);
                }
            }
        }
		return c;
	}

    // Strassen multiplication
	public Matrix multStrassen(Matrix b, int cutoff){
        //TODO
        if (cutoff >= this.n) {
            return this.mult(b); // POPRAVI
        }

        Matrix[] subA = new Matrix[4];
        Matrix[] subB = new Matrix[4];

        subA[0] = this.getSubmatrix(0, 0, this.n / 2);
        subA[1] = this.getSubmatrix(0, this.n / 2, this.n / 2);
        subA[2] = this.getSubmatrix(this.n / 2, 0, this.n / 2);
        subA[3] = this.getSubmatrix(this.n / 2,  this.n / 2, this.n / 2);

        subB[0] = b.getSubmatrix(0, 0, this.n / 2);
        subB[1] = b.getSubmatrix(0, this.n / 2, this.n / 2);
        subB[2] = b.getSubmatrix(this.n / 2, 0, this.n / 2);
        subB[3] = b.getSubmatrix(this.n / 2,  this.n / 2, this.n / 2);

        Matrix[] m = new Matrix[7];
        m[0] = (subA[0].sum(subA[3])).multStrassen(subB[0].sum(subB[3]), cutoff);
        m[1] = (subA[2].sum(subA[3])).multStrassen(subB[0], cutoff);
        m[2] = subA[0].multStrassen(subB[1].sub(subB[3]), cutoff);
        m[3] = subA[3].multStrassen(subB[2].sub(subB[0]), cutoff);
        m[4] = (subA[0].sum(subA[1])).multStrassen(subB[3], cutoff);
        m[5] = (subA[2].sub(subA[0])).multStrassen(subB[0].sum(subB[1]), cutoff);
        m[6] = (subA[1].sub(subA[3])).multStrassen(subB[2].sum(subB[3]), cutoff);

        for (int i = 0; i < 7; i++) {
            System.out.printf("m%d: %d\n", i + 1, m[i].vsota());
        }

        Matrix c = new Matrix(n);
        
        Matrix c11 = m[0].sum(m[3]).sub(m[4]).sum(m[6]);
        Matrix c12 = m[2].sum(m[4]);
        Matrix c21 = m[1].sum(m[3]);
        Matrix c22 = m[0].sub(m[1]).sum(m[2]).sum(m[5]);

        c.putSubmatrix(0, 0, c11);
        c.putSubmatrix(0, this.n / 2, c12);
        c.putSubmatrix(this.n / 2, 0, c21);
        c.putSubmatrix(this.n / 2, this.n / 2, c22);

        return c;
	}

    /*  
    m1 = (a11+a22)(b11+b22)
	m2 = (a21+a22)b11
	m3 = a11(b12-b22)
	m4 = a22(b21-b11)
	m5 = (a11+a12)b22
	m6 = (a21-a11)(b11+b12)
    m7 = (a12-a22)(b21+b22)
    
    c11 = m1+m4-m5+m7
	c12 = m3+m5
	c21 = m2+m4
    c22 = m1-m2+m3+m6 
    */

    public int vsota() {
        int sum = 0;
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                sum += this.v(i, j);
            }
        }
        return sum;
    }
}




public class Izziv6{

	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
        int cutoff = sc.nextInt();

        Matrix a = new Matrix(n);
        Matrix b = new Matrix(n);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int val = sc.nextInt();
                a.setV(i, j, val);
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int val = sc.nextInt();
                b.setV(i, j, val);
            }
        }

        Matrix c = a.multStrassen(b, cutoff);
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(c.v(i, j) + " ");
            }
            System.out.println();
        }
	}

}
