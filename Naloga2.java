import java.util.Scanner;

class Number {

    private char[] number;
    private int[] value;
    private int base;

    public Number(String s, int base) {
        char[] number = new char[s.length()];
        int[] value = new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            number[i] = c;
            value[i] = getIntValue(c);
        }
        this.number = number;
        this.value = value;
        this.base = base;
    }

    public int getLength() {
        return this.number.length;
    }

    public char[] getNumber() {
        return this.number;
    }

    public String numberToString() {
        return new String(this.getNumber());
    }

    private int getIntValue(char c) {
        int value = (int) c;
        if (value - 48 > 9) {
            return (value - 87);
        }
        return (value - 48);
    }

    private char getCharValue(int i) {
        if (i <= 9) {
            return (char) (i + 48);
        } else {
            return (char) (i + 87);
        }
    }

    public Number plus(Number n) {
        int carry = 0;
        Number bigger = this.getLength() >= n.getLength() ? this : n;
        int index = 1;
        String rez = "";

        while (bigger.getLength() - index >= 0) {
            int i1 = this.getLength() - index >= 0 ? this.getLength() - index : -1;
            int i2 = n.getLength() - index >= 0 ? n.getLength() - index : -1;
            int val = 0;
            if (i1 == -1)
                val = 0 + n.value[i2] + carry; 
            else if (i2 == -1)
                val = this.value[i1] + 0 + carry;
            else
                val = this.value[i1] + n.value[i2] + carry;

            carry = val / this.base;
            val = val % this.base;
            String s = String.valueOf(getCharValue(val));
            rez = s + rez;
            index++;
        }

        if (carry != 0) rez =  String.valueOf(getCharValue(carry)) + rez;

        return new Number(rez, this.base);
    }

    // works if Number this is bigger or equal to Number n
    public Number minus(Number n) {
        int carry = 0;
        int index = 1;
        String rez = "";

        while (this.getLength() - index >= 0) {
            int i1 = this.getLength() - index;
            int i2 = n.getLength() - index >= 0 ? n.getLength() - index : -1;
            int val = 0;
            if (i2 == -1)
                val = this.value[i1] - (0 + carry);
            else
                val = this.value[i1] - (n.value[i2] + carry);

            if (val < 0) {
                val = this.base + val;
                carry = 1;
            } else {
                carry = 0;
            }

            String s = String.valueOf(getCharValue(val));
            rez = s + rez;
            index++;
        }

        boolean cut = false;
        for (int i = 0; i < rez.length(); i++) {
            if (rez.charAt(i) != '0') {
                rez = rez.substring(i, rez.length());
                cut = true;
                break;
            }
        }

        if (!cut) rez = "0";

        return new Number(rez, this.base);
    }

    // BASIC MULTIPLICATION

    public Number basicMultiplication(Number st, boolean trace) {
        int carry = 0;
        int index = 1;
        String rez = "";

        String temp = "";
        Number current = new Number("0", this.base);

        for (int i = 0; i < st.getLength(); i++) {
            rez = "";
            carry = 0;
            if (st.value[i] == 0) { 
                rez = "";
            } else {
                for (int j = this.getLength() - 1; j >= 0 ; j--) {
                    int val = (st.value[i] * this.value[j]) + carry;
                    carry = val / this.base;
                    val = val % this.base;
                    String s = String.valueOf(getCharValue(val));
                    rez = s + rez;
                }
            }

            if (carry != 0) rez =  String.valueOf(getCharValue(carry)) + rez;

            if (trace && rez.equals("")) System.out.println("0");
            if (trace && !rez.equals("")) System.out.println(rez);
            temp = temp + "0";
            // System.out.println("Shifted = " + temp);
            Number to_add = new Number(rez, this.base);
            current = new Number(temp, this.base);
            current = current.plus(to_add);
            temp = current.numberToString();
            // System.out.println("New current = " + temp);
        }

        if (trace) {
            for (int i = 0; i < temp.length(); i++) {
                System.out.print("-");
            }
            System.out.println();
        }

        // System.out.println(temp);
        return current;
    }

    /*  RETURN HALF
        return half of a number with certain length
        if number is 234 and the required length is 12
        it will return [1] = 0; [0] = 234
        where as if the length is 4 it will return
        [1] = 2; [0] = 34 
    */
    private Number[] returnHalf(int len) {
        String first = "";
        String second = "";

        for (int i = 1; i <= len / 2; i++) {
            int index1 = this.getLength() - (len/2) + i - 1;
            if (index1 >= 0 && !(first.equals("") && this.number[index1] == '0')) {
                first = first + this.number[index1];
            }

            int index2 = this.getLength() - (i + (len / 2));
            if (index2 < 0) {
                continue;
            }

            second = this.number[index2] + second;
        }
        if (first.equals("")) first = "0";
        if (second.equals("")) second = "0";

        // System.out.println("divide = " + first + " " + second);

        return new Number[]{new Number(first, this.base), new Number(second, this.base)};
    }

    // shift the number for n to the left
    private Number shiftN(int n) {
        String num = this.numberToString();
        if (num.equals("0")) return this;
        for (int i = 0; i < n; i++) {
            num = num + "0";
        }
        return new Number(num, this.base);
    }

    // DIVIDE AND COQUER MULTIPLICATION

    public Number divideAndQonquer(Number st) {

        System.out.println(this.numberToString() + " " + st.numberToString());

        if (this.getLength() == 1) {
            if (this.value[0] == 0) {
                return new Number("0", this.base);
            }
            // basic multiplication
            return st.basicMultiplication(this, false);
        }

        if (st.getLength() == 1) {
            if (st.value[0] == 0) {
                return new Number("0", st.base);
            }
            // basic multiplication
            return this.basicMultiplication(st, false);
        }

        int len = this.getLength() >= st.getLength() ? this.getLength() : st.getLength();
        len = len % 2 == 0 ? len : len + 1;
        Number[] a = this.returnHalf(len); // a1a0 ... index 1 is the first half! index 0 is the second half!
        Number[] b = st.returnHalf(len); // b1b0 ... index 1 is the first half! index 0 is the second half!

        // a0*b0
        Number third = (a[0].divideAndQonquer(b[0]));
        System.out.println(third.numberToString());
        // (a0*b1 + a1*b0)*base^n/2
        Number second1 = a[0].divideAndQonquer(b[1]);
        System.out.println(second1.numberToString());
        Number second2 = a[1].divideAndQonquer(b[0]);
        System.out.println(second2.numberToString());
        Number second = (second1.plus(second2)).shiftN(len / 2);
        // a1*b1*base^n
        Number first = (a[1].divideAndQonquer(b[1]));
        System.out.println(first.numberToString());
        first = first.shiftN(len);
        
        // a1*b1*base^n + (a0*b1 + a1*b0)*base^n/2 + a0*b0
        return ((first.plus(second)).plus(third));
    }

    // KARATSUBA MULTIPLICATION

    public Number karatsuba(Number st) {
        
        System.out.println(this.numberToString() + " " + st.numberToString());

        if (this.getLength() == 1) {
            if (this.value[0] == 0) {
                return new Number("0", this.base);
            }
            // basic multiplication
            return st.basicMultiplication(this, false);
        }

        if (st.getLength() == 1) {
            if (st.value[0] == 0) {
                return new Number("0", st.base);
            }
            // basic multiplication
            return this.basicMultiplication(st, false);
        }

        int len = this.getLength() >= st.getLength() ? this.getLength() : st.getLength();
        len = len % 2 == 0 ? len : len + 1;
        Number[] a = this.returnHalf(len); // a1a0 ... index 1 is the first half! index 0 is the second half!
        Number[] b = st.returnHalf(len); // b1b0 ... index 1 is the first half! index 0 is the second half!

        // a0*b0
        Number first = a[0].karatsuba(b[0]);
        System.out.println(first.numberToString());
        // a1*b1
        Number second = a[1].karatsuba(b[1]);
        System.out.println(second.numberToString());
        // (a0+a1)*(b0+b1)
        Number third = (a[0].plus(a[1])).karatsuba(b[0].plus(b[1]));
        System.out.println(third.numberToString());
        
        // a1*b1*base^n + ((a0+a1)*(b0+b1) - a1*b1 - a0*b0)*base^n/2 + a0*b0
        return ((second.shiftN(len)).plus(((third.minus(second)).minus(first)).shiftN(len / 2))).plus(first);
    }
}

public class Naloga2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String alg = sc.next();
        int base = sc.nextInt();
        Number s1 = new Number(sc.next(), base);
        Number s2 = new Number(sc.next(), base);

        sc.close();

        if (alg.equals("os")) {
            System.out.println(s1.basicMultiplication(s2, true).numberToString());
        } else if (alg.equals("dv")) {
            System.out.println(s1.divideAndQonquer(s2).numberToString());
        } else if (alg.equals("ka")) {
            System.out.println(s1.karatsuba(s2).numberToString());
        }
        
    }

}
