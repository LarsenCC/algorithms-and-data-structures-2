import java.util.Scanner;

class Matrixx {

	private int[][] matrix;

    private int n; //only square matrices
    private int m;

	public Matrixx(int n, int m){
        this.n = n;
        this.m = m;
		this.matrix = new int[n][m];
	}

    //set value at i,j
	public void setValue(int i, int j, int val){
		this.matrix[i][j] = val;
	}

    // get value at index i,j
	public int getValue(int i, int j){
		return this.matrix[i][j];
	}

    public int getM () {
        return this.m;
    }

    public int getN() {
        return this.n;
    }

    // return a square submatrix from this
	public Matrixx getSubmatrix(int startRow, int startCol, int n, int m){
		Matrixx subM = new Matrixx(n, m);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
                if (this.getN() <= startRow + i || this.getM() <= startCol + j) {
                    subM.setValue(i, j, 0);
                } else {
                    subM.setValue(i, j, this.getValue(startRow + i, startCol + j));
                }
            }
        }
		return subM;
	}

    // write this matrix as a submatrix from b (useful for the result of multiplication)
	public void putSubmatrix(int startRow, int startCol, Matrixx b){
		for (int i = 0; i < b.getN(); i++) {
            for (int j = 0; j < b.getM(); j++) {
                this.setValue(startRow + i, startCol + j, b.getValue(i,j));
            }
        }	
	}

    // matrix addition
    public Matrixx plus(Matrixx b){
		Matrixx c = new Matrixx(this.getN(), this.getM());
		for(int i = 0; i < this.getN(); i++){
			for(int j = 0; j < this.getM(); j++){
				c.setValue(i, j, this.getValue(i, j) + b.getValue(i, j));
			}
		}
		return c;
	}

    // matrix subtraction
	public Matrixx minus(Matrixx b){
		Matrixx c = new Matrixx(this.getN(), this.getM());
		for(int i = 0; i < this.getN(); i++){
			for(int j = 0; j < this.getM(); j++){
				c.setValue(i, j, this.getValue(i, j) - b.getValue(i, j));
			}
		}
		return c;
	}

    // BASIC MATRIX MULTIPLICATION
    
	public Matrixx basicMatrixMultiplication(Matrixx b){
		Matrixx c = new Matrixx(this.getN(), b.getM());

        for (int i = 0; i < c.getN(); i++) {
            for (int j = 0; j < c.getM(); j++) {
                for (int k = 0; k < this.getM(); k++) {
                    int val = this.getValue(i, k) * b.getValue(k, j);
                    c.setValue(i, j, c.getValue(i, j) + val);
                }
            }
        }
		return c;
    }
    
    // BLOCK MATRIX MULTIPLICATION

    public Matrixx blockMatrixMultiplication(Matrixx b, int block_size){
        int newN = this.getN() % block_size != 0 ? ((this.getN() / block_size) + 1) * block_size : this.getN();
        int newM = b.getM() % block_size != 0 ? ((b.getM() / block_size) + 1 ) * block_size : (b.getM());
		Matrixx c = new Matrixx(newN, newM);

        for (int i = 0; i < c.getN(); i = i + block_size) {
            for (int j = 0; j < c.getM(); j = j + block_size) {
                for (int k = 0; k < this.getM(); k = k + block_size) {
                    Matrixx Aik = this.getSubmatrix(i, k, block_size, block_size);
                    Matrixx Bkj = b.getSubmatrix(k, j, block_size, block_size);

                    Matrixx val = Aik.basicMatrixMultiplication(Bkj);
                    System.out.println(val.signiture());

                    c.putSubmatrix(i, j, c.getSubmatrix(i, j, block_size, block_size).plus(val));
                }
            }
        }

		return c.getSubmatrix(0, 0, this.getN(), b.getM());
    }
    
    // DIVIDE AND QONCUER MATRIX MULTIPLICATION

    public Matrixx divideAndQonquer(Matrixx b, int cutoff) {
        if (cutoff >= this.getN()) {
            return this.basicMatrixMultiplication(b);
        }

        Matrixx[] subA = new Matrixx[4];
        Matrixx[] subB = new Matrixx[4];

        subA[0] = this.getSubmatrix(0, 0, this.getN() / 2, this.getN() / 2); // A11
        subA[1] = this.getSubmatrix(0, this.getN() / 2, this.getN() / 2, this.getN() / 2); // A12
        subA[2] = this.getSubmatrix(this.getN() / 2, 0, this.getN() / 2, this.getN() / 2); // A21
        subA[3] = this.getSubmatrix(this.getN() / 2,  this.getN() / 2,this.getN() / 2, this.getN() / 2); //A22

        subB[0] = b.getSubmatrix(0, 0, this.getN() / 2, this.getN() / 2); // B11
        subB[1] = b.getSubmatrix(0, this.getN() / 2, this.getN() / 2, this.getN() / 2); // B12
        subB[2] = b.getSubmatrix(this.getN() / 2, 0, this.getN() / 2, this.getN() / 2); // B21
        subB[3] = b.getSubmatrix(this.getN() / 2,  this.getN() / 2, this.getN() / 2, this.getN() / 2); // B22
        
        Matrixx A11B11 = subA[0].divideAndQonquer(subB[0], cutoff);
        System.out.println(A11B11.signiture());
        Matrixx A12B21 = subA[1].divideAndQonquer(subB[2], cutoff);
        System.out.println(A12B21.signiture());
        Matrixx A11B12 = subA[0].divideAndQonquer(subB[1], cutoff);
        System.out.println(A11B12.signiture());
        Matrixx A12B22 = subA[1].divideAndQonquer(subB[3], cutoff);
        System.out.println(A12B22.signiture());
        Matrixx A21B11 = subA[2].divideAndQonquer(subB[0], cutoff);
        System.out.println(A21B11.signiture());
        Matrixx A22B21 = subA[3].divideAndQonquer(subB[2], cutoff);
        System.out.println(A22B21.signiture());
        Matrixx A21B12 = subA[2].divideAndQonquer(subB[1], cutoff);
        System.out.println(A21B12.signiture());
        Matrixx A22B22 = subA[3].divideAndQonquer(subB[3], cutoff);
        System.out.println(A22B22.signiture());

        Matrixx c = new Matrixx(this.getN(), this.getN());
        
        Matrixx c11 = A11B11.plus(A12B21);
        Matrixx c12 = A11B12.plus(A12B22);
        Matrixx c21 = A21B11.plus(A22B21);
        Matrixx c22 = A21B12.plus(A22B22);

        c.putSubmatrix(0, 0, c11);
        c.putSubmatrix(0, this.getN() / 2, c12);
        c.putSubmatrix(this.getN() / 2, 0, c21);
        c.putSubmatrix(this.getN() / 2, this.getN() / 2, c22);

        return c;
    }
    
    // STRASSEN MATRIX MULTIPLICATION

	public Matrixx strassen(Matrixx b, int cutoff){
        if (cutoff >= this.getN()) {
            return this.basicMatrixMultiplication(b);
        }

        Matrixx[] subA = new Matrixx[4];
        Matrixx[] subB = new Matrixx[4];

        subA[0] = this.getSubmatrix(0, 0, this.getN() / 2, this.getN() / 2); // A11
        subA[1] = this.getSubmatrix(0, this.getN() / 2, this.getN() / 2, this.getN() / 2); // A12
        subA[2] = this.getSubmatrix(this.getN() / 2, 0, this.getN() / 2, this.getN() / 2); // A21
        subA[3] = this.getSubmatrix(this.getN() / 2,  this.getN() / 2,this.getN() / 2, this.getN() / 2); //A22

        subB[0] = b.getSubmatrix(0, 0, this.getN() / 2, this.getN() / 2); // B11
        subB[1] = b.getSubmatrix(0, this.getN() / 2, this.getN() / 2, this.getN() / 2); // B12
        subB[2] = b.getSubmatrix(this.getN() / 2, 0, this.getN() / 2, this.getN() / 2); // B21
        subB[3] = b.getSubmatrix(this.getN() / 2,  this.getN() / 2, this.getN() / 2, this.getN() / 2); // B22

        Matrixx c = new Matrixx(this.getN(), this.getN());
        
        Matrixx P1 = subA[0].strassen(subB[1].minus(subB[3]), cutoff);
        System.out.println(P1.signiture());
        Matrixx P2 = (subA[0].plus(subA[1])).strassen(subB[3], cutoff);
        System.out.println(P2.signiture());
        Matrixx P3 = (subA[2].plus(subA[3])).strassen(subB[0], cutoff);
        System.out.println(P3.signiture());
        Matrixx P4 = subA[3].strassen(subB[2].minus(subB[0]), cutoff);
        System.out.println(P4.signiture());
        Matrixx P5 = (subA[0].plus(subA[3])).strassen(subB[0].plus(subB[3]), cutoff);
        System.out.println(P5.signiture());
        Matrixx P6 = (subA[1].minus(subA[3])).strassen(subB[2].plus(subB[3]), cutoff);
        System.out.println(P6.signiture());
        Matrixx P7 = (subA[0].minus(subA[2])).strassen(subB[0].plus(subB[1]), cutoff);
        System.out.println(P7.signiture());

        Matrixx c11 = P5.plus(P4).minus(P2).plus(P6);
        Matrixx c12 = P1.plus(P2);
        Matrixx c21 = P3.plus(P4);
        Matrixx c22 = P1.plus(P5).minus(P3).minus(P7);

        c.putSubmatrix(0, 0, c11);
        c.putSubmatrix(0, this.getN() / 2, c12);
        c.putSubmatrix(this.getN() / 2, 0, c21);
        c.putSubmatrix(this.getN() / 2, this.getN() / 2, c22);

        return c;
	}

    public int signiture() {
        int sum = 0;
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                sum += this.getValue(i, j);
            }
        }
        return sum;
    }

    public void printMatrix() {
        System.out.printf("DIMS: %dx%d\n", this.n, this.m);
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.m; j++) {
                System.out.print(getValue(i, j) + " ");
            }
            System.out.println();
        }
    }
}

public class Naloga2matrix {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String alg = sc.next();
        int param = 0;
        if (!alg.equals("os")) {
            param = sc.nextInt();
        }

        int n1 = sc.nextInt();
        int m1 = sc.nextInt();

        Matrixx a = new Matrixx(n1, m1);

        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < m1; j++) {
                a.setValue(i, j, sc.nextInt());
            }
        }

        int n2 = sc.nextInt();
        int m2 = sc.nextInt();

        Matrixx b = new Matrixx(n2, m2);

        for (int i = 0; i < n2; i++) {
            for (int j = 0; j < m2; j++) {
                b.setValue(i, j, sc.nextInt());
            }
        }

        sc.close();

        if (alg.equals("os")) {
            (a.basicMatrixMultiplication(b)).printMatrix();
        } else if (alg.equals("bl")) {
            (a.blockMatrixMultiplication(b, param)).printMatrix();
        } else if (alg.equals("dv")) {
            int max = Math.max(n1, Math.max(m1, Math.max(n2, m2)));
            int k = (int) Math.ceil(Math.log(max) / Math.log(2));
            int newDim = (int) Math.pow(2, k);
            
            Matrixx A = new Matrixx(newDim, newDim);
            Matrixx B = new Matrixx(newDim, newDim);

            A.putSubmatrix(0, 0, a);
            B.putSubmatrix(0, 0, b);

            param = param >= 1 ? param : 1;
            (A.divideAndQonquer(B, param)).printMatrix();
        } else if (alg.equals("st")) {
            int max = Math.max(n1, Math.max(m1, Math.max(n2, m2)));
            int k = (int) Math.ceil(Math.log(max) / Math.log(2));
            int newDim = (int) Math.pow(2, k);
            
            Matrixx A = new Matrixx(newDim, newDim);
            Matrixx B = new Matrixx(newDim, newDim);

            A.putSubmatrix(0, 0, a);
            B.putSubmatrix(0, 0, b);

            param = param >= 1 ? param : 1;
            (A.strassen(B, param)).printMatrix();
        }
    }
}